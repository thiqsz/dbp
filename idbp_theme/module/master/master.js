// Filename: views/project/list
define([
  'jquery',
  'underscore',
  'backbone',
  'text!module/master/master_template.html',
  'module/master/side/menuView',
  'module/rack/rackView'
], function($, _, Backbone,template,MenuView,RackView){

    var MasterView = Backbone.View.extend({
        el: $('#container'),
        
        initialize:function(){
            this.listenTo( Backbone, 'change-category', function (category) {
                  this.loadCategoryRack(category);
            }, this );
        },
        
        loadMenu:function(){
            console.log('laod Menu..');
            this.menuView = new MenuView();
            this.menuView.render();
        },
        
        
        loadEmptyRack:function(){
            console.log('Empty Rack... please check internet con');
            
        },
        
        loadCategoryRack:function(cat){
//            console.log(cat);
            if(typeof rackView=='undefined'){
                rackView = new RackView($('.content'));
            }
//            delete rackView;
            rackView.render(cat);
        },
        
        render: function(){
            // Using Underscore we can compile our template with data
            var data={};
            var compiledTemplate = _.template(template);
            this.$el.replaceWith(compiledTemplate());
            this.loadMenu();
            this.loadCategoryRack("all");//this.loadCategoryRack(<DEFAULT CATEGORY>);
            this.el = $('body');
        },
        test:function(){
            console.log('test MasterView'); 

        }
 
  });
  // Our module now returns our view
  return MasterView;
});