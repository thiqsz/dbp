<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Publishers extends Model {

	protected $table = 'dbp_publishers';
	protected $primaryKey = 'pub_id';

}
