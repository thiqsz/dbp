<?php namespace App\Http\Controllers;

use Session;
use Validator;
use Input;
use Redirect;
use App\Books;
use App\Author;
use App\Category;
use App\BookLevel;
use App\Publishers;
use App\DBPStatus;
use App\BooksImages;
use App\DBPUsers;
use View;
use Response;
use File;
use Request;
use Carbon;
use DB;

class DashboardController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {
        $booknew = Books::orderBy('id', 'desc')->take(10)->get();
        $book = Books::count();
        $booknew2weeks = Books::where('created_at', '>', DB::raw('DATE_SUB(now(), INTERVAL 2 WEEK)'))->count();
        $author = Author::count();
        $publisher = Publishers::count();
        $users = DBPUsers::count();
        return View::make('index')
            ->nest('header_script', 'header_script')
            ->nest('side_menu', 'side_menu')
            ->nest('body', 'body_dashboard', array('author' => $author, 'books' => $book, 'publisher' => $publisher, 'booknew' => $booknew, 'booknew2week' => $booknew2weeks, 'usercount' => $users))
            ->nest('footer_script', 'footer_script');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }

}
