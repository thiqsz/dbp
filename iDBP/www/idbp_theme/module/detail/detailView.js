// Filename: RackView
//Ya Hayyu Ya Qayyum birahmatika astaghitsu, ashlih lii sya’ni kullahu walaa takilni ila nafsi thorfata ‘ain
define([
  'jquery',
  'underscore',
  'backbone',
  'blazy',
  'text!module/detail/detail_template.html',
  'text!module/detail/temp/readSample_template2.html',
  'text!module/detail/temp/otherBook_template.html',
  'text!module/master/others/noInternet_template.html',
  'text!module/master/others/notification_template.html',
  'module/Model/BookDetailsModel',
  'module/Model/BookCollection',
   
], function($, _, Backbone,Blazy,template,readSampleTemplate,otherBookTemplate,noInternetTemplate,notificationTemplate,BookDetails,BookList){
    
    var DetailView = Backbone.View.extend({
        el: '#main',
        
        initialize:function(){
            console.log('laod Detail Content..');
             this.bookListOther = new BookList();
           
        },

         events:{
            "click #btnMore":"gotoCategoryList",
            "click .otherBook":"selectOneOtherBook",
            "click #btnDownload":"downloadSample",
            "click #btnBack":"gobacktoRackView"
        },
        
      
        render: function(bookId,category){            
            console.log('fetch book data..');
            this.bookId = bookId;
            this.category =  category;
            //fetch data
            that= this;
            var onDataHandler = function(collection){
                that.renderBookDetails();
            }
            var onErrorHandler = function(collection, response, options) {
                console.log("error - no book to show");
                 if(response.status==404){
                  console.log("error page request"); 
                  that.renderNotification(resource.msg404Error);
                }else{
                  that.loadInternetError();   
                }
            }
            this.book = new BookDetails(this.bookId);
            this.book.fetch({ success : onDataHandler, error:onErrorHandler,dataType: "jsonp"});
//            this.book.fetch({ success : onDataHandler, error:onErrorHandler});
            
            this.$('#btnLogin').click(function(){
                Backbone.trigger('login-listener', that);
            });
            
            
     
        },
        
        //fetch data success handler
        renderBookDetails:function(){
            // Using Underscore we can compile our template with data
            var data={
                        book:this.book.toJSON()
                     };
            
            var compiledTemplate = _.template(template);
            var sampleTemplate = _.template(readSampleTemplate);
            
            this.$el.html(compiledTemplate(data));
            $('#btnMore').hide();
            $('.other').hide();
            
            
            if(data.book.samples.length>0){
              $('#sample-container').html(sampleTemplate({samples:data.book.samples}));
            }
            
            this.loadImage();
            this.renderOtherBook();
            $(window).scrollTop(0);
        },
         
        selectOneOtherBook:function(e){
//            console.log($(e.currentTarget).data('bookid'));
              this.bookId = $(e.currentTarget).data('bookid');
              this.render(this.bookId,this.category);
        },
        
        renderOtherBook:function(){
            that=this;
            this.qBook=0;
           
            
            this.bookListOther.setValue(this.category,0,this.bookId);
            
            var onDataHandler = function(collection){
                
                console.log(collection);
                var dataOther={
                        otherBooks:collection.toJSON()
                     };
                var compiledTemplate = _.template(otherBookTemplate);
                $('#other_book').html(compiledTemplate(dataOther));
               
                console.log("other books");
                console.log(dataOther.otherBooks);
                
                if(dataOther.otherBooks.length>0){
                    $('#btnMore').show();
                    $('.other').show();
                }
                if(dataOther.otherBooks.length>3){
                    $('#btnMore').show();
                }else{
                    $('#btnMore').hide();
                }
                
                that.loadImage();
                that.qBook=0;
            }
            
            var onErrorHandler = function(collection, response, options) {
                console.log("error");
                $('#btnMore').hide();
            }
            
            this.bookListOther.fetch({ error:onErrorHandler, success : onDataHandler,reset:false});
        },
        loadInternetError:function(){
            var temp = _.template(noInternetTemplate);
            $('.row').html(temp());
            $(window).scrollTop(0);
        },
        renderNotification:function(msg){
               var data={
                    message:msg
               };
               var compiledTemplate = _.template(notificationTemplate);
               $('#notification').html(compiledTemplate(data));
               $('.toast').addClass('toast-show');
               toastHide(6000,$(this));
        },
        
        loadImage:function(){
          blazy = new Blazy({
                container: '.bookthumb' ,// Default is window
                success: function(){
                    console.log('loading image');
                },
                error: function(ele, msg){
                    console.log(msg);
                }

            });
            
        },
        
        gotoCategoryList:function(){
            Backbone.trigger('change-category', this.category );//Backbone.trigger('change-category', <CATEGORY> );
        },
        
        gobacktoRackView:function(){
            Backbone.trigger('change-category', sessionStorage.getItem('current-category'));//Backbone.t
        },
       
        downloadSample:function(){
            that=this;
            var downloadType = [ "PDF","EPUB" ];
            var fileTransfer = new FileTransfer();
            doFileDownload=function(no){
                type = downloadType[no-1];
                if(type!=='undefined'){
                    type=type.toLowerCase();
                  
                    var fileURL = "cdvfile://localhost/persistent/naskah-iDBP/idbpSampel-"+that.bookId+"."+type;
                    var uri = encodeURI(resource.rootPublicUrl+resource.dowload+that.bookId+'/'+type);
                    cordova.plugins.pDialog.init({
                        progressStyle : 'HORIZONTAL', 
                        title: 'Sebentar...', 
                        message : 'Sedang menghubungi ke sistem pelayan'
                    });
                    
                    
                    //            
                    fileTransfer.onprogress = function(progressEvent) {
                       
                        if (progressEvent.lengthComputable) {
                            var perc = Math.floor(progressEvent.loaded / progressEvent.total * 100);
                            console.log(perc + "% dimuat turun...");


                            cordova.plugins.pDialog.setProgress(perc);
                            
                            if(perc ==100){
                             cordova.plugins.pDialog.dismiss();
                             cordova.plugins.fileOpener2.open(fileURL,'application/'+type, 
                                { 
                                    error : function(e) { 
                                        console.log('Error status: ' + e.status + ' - Error message: ' + e.message);
                                        navigator.notification.confirm(resource.msgPDFError, null, resource.msgTitle, ["OK"]);
                                    },
                                    success : function () {
                                        console.log('file opened successfully');                
                                    }
                                }
                              );
                            }
                        } else {
        //                    if(statusDom.innerHTML == "") {
        //                        statusDom.innerHTML = "Loading";
        //                    } else {
        //                        statusDom.innerHTML += ".";
        //                    }
                        }
                    };
                    
                    fileTransfer.download(
                        uri,
                        fileURL,
                        function(entry) {
                            console.log("download complete: " + entry.fullPath);
                        },
                        function(error) {
                            console.log("download error source " + error.source);
                            console.log("download error target " + error.target);
                            console.log("upload error code" + error.code);
                        },
                        false,
                        {
                            headers: {
                                "Authorization": ""//
                            }
                        }
                    );
                    
                    
                }
            };
            navigator.notification.confirm(resource.msgDownload, doFileDownload,resource.downloadTitle, downloadType);            
            
            
        },
        
       
        test:function(){
            alert('test');
            console.log('test DetailView');  
        }

  });
  // Our module now returns our view
  return DetailView;
});