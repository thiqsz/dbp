// Filename: views/project/list
define([
  'jquery',
  'underscore',
  'backbone',
  'text!module/master/master_template.html',
  'text!module/master/others/login_template.html',
  'text!module/master/others/forget_template.html', 
  'text!module/master/others/register_template.html', 
  'text!module/master/others/beforeLoginMenu_template.html', 
  'text!module/master/others/afterLoginMenu_template.html', 
  'text!module/master/side/rightMenu_template.html', 
  'module/master/side/menuView',
  'module/rack/rackView',
  'module/detail/detailView',
  'module/Model/UserModel',
  'module/Model/TestModel',
], function($, _, Backbone,template,loginTemplate,forgetTemplate,registerTemplate,beforeLoginTemplate,afterLoginTemplate,rightMenuTemplate,MenuView,RackView,DetailView,User,Test){

    var MasterView = Backbone.View.extend({
        el: $('#container'),
        
        initialize:function(){ 
            that= this;
            this.listenTo(Backbone, 'change-category', function (cat) {
                 console.log('Master Listen');
                  this.loadCategoryRack(cat);
            }, this);
            
            this.bindEvents();
            
            //state
            sessionStorage.setItem('current-category', 'all');
            
              this.hoho =0;
        },
        
        events:{
            'click #btnLogin':'userLogin',
            'click #btnDoRegister':'doRegister',
            'click #btnLogout': 'userLogout',
            'click #btnForgotPassword':'loadForgetPass'
            //'click #btnForgotPassword':'userForgotPassword'
        },
        
        bindEvents: function() {
//            document.addEventListener("backbutton", this.onBackKeyDown, false);
            var ref = document;
            that=this;
            
            ref.addEventListener("backbutton", function () {
                //ttd: have to restructure back
                alert(that instanceof DetailView);
                console.log(that);
                
                if($('.modal').hasClass('in')){
                        return false;
                    }
                
                if(that instanceof DetailView){
                    
                    console.log('DetailView');
                    Backbone.trigger('change-category',that.category);
                }
                if(that instanceof RackView){
                    
                    console.log('RackView');
                   
                    Backbone.trigger('change-category',that.category);
                    
                }
            });
        },
       
        loadMenu:function(){
            console.log('laod Menu..');
            this.menuView = new MenuView();
            this.menuView.render();
            
        },
        
        loadBeforeLoginTopMenu:function(){
             
            var topTemp = _.template(beforeLoginTemplate);
            $('#top-menu').html(topTemp);
        },
        
        loadCategoryRack:function(category){
            this.rackView.render(category);
        },
                
        loadModal:function(){
            that =this;
            var compiledTemplate = _.template(loginTemplate);
            $('#login_content').html(compiledTemplate());
            
            //load register
            var regTemplate = _.template(registerTemplate);
            $('#register_content').html(regTemplate());
            

        },
        
        loadForgetPass:function(){
            that = this;
            var compiledTemplate = _.template(forgetTemplate);
            $('#login_content').html(compiledTemplate());
        },
        
        loadProfile:function(details){
            
            that = this;
             
            var rightMenuTemp = _.template(rightMenuTemplate);
            $('#profile').html(rightMenuTemp);
            
            $('.menu-top-user').text(details.name);
            
           
        },
        
        
        userLogin:function(){
            console.log('login');
            
             that=this;
            $.ajax({
                url:resource.rootUrl+resource.login,
                type:'POST',
                dataType:"json",
                data: {"username":$('#login-email').val(),"password":$('#login-password').val()},
                xhrFields: {
                    withCredentials: true
                },
                success:function (data) {
//                    console.log(["Login request details: ", data]);
   
                    if(data.error) {  // If there is an error, show the error messages
                        console.log('error');
                    }
                    else { // If not, send them back to the home page
                        console.log(data);
                        console.log(data['login']);
                       
                        if(data.login=='OK'&&data.status==200){
                            //authorized
                            var afterLoginTemp = _.template(afterLoginTemplate);
                            $('#top-menu').html(afterLoginTemp);
                            window.parent.closeModal('#modal-login');
                            that.loadProfile(data.details);
                           
                        }else{
                            navigator.notification.confirm(resource.msgWrongAuth, null, resource.msgTitle, ["OK"]);
                            console.log('error login');   
                        }
                    }
                }
            });
            
        },
        
        userLogout:function(){
            //logout should be able to trigger event in offline mode
                that=this;
                $.ajax({
                    url:resource.rootUrl+resource.logout,
                    type:'GET',
                    xhrFields: {
                        withCredentials: true
                    },
                    success:function (data) {
                        
                        if(data.error) {  // If there is an error, show the error messages
                            console.log('error - logout');
                        }
                        else { // If not, send them back to the home page
//                            alert(data);
                            //do logout things
                            
//                            $('#profile').html('');
                           mReset();
                           that.loadBeforeLoginTopMenu();
                            
                        }
                    },
                    error:function(err){
                        
                     alert('error');   
                    }
                });
        },
        
        userForgotPassword:function(){
            
            email = $('#login-email').val();

            var re = /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
           
            if(!re.test(email)){
                alert('masukakanla yg betul kat ruang email tu');
                return;
            }
            
            that=this;
                $.ajax({
                    url:resource.rootUrl+resource.forgotPassword,
                    type:'POST',
                    data:{email:email},
                    success:function (data) {
                        console.log(data);
                        if(data.error) {  // If there is an error, show the error messages
                            console.log('error - forgot password');
                        }
                        else { 
                            console.log(data.response);
                            if(data.response == 'OK'){
                             alert(data.message);
                             console.log(data.message);   
                            }
                            
                        }
                    },
                    error:function(err){
                     console.log(err);
                     alert('error-'+err);   
                    }
                });
        },
        
        gotoRegister:function(){
           
        },
        
        doRegister:function(){
            console.log("register");
            that=this;
            $.ajax({
                url:resource.rootUrl+resource.register,
                type:'POST',
                data:{
                    "username":$('#float-username').val(),
                    "name":$('#float-fullname').val(),
                    "email":$('#float-email').val(),
                    "password":$('#float-password').val(),
                    "password_confirmation":$('#float-password_confirmation').val(),
                },
                xhrFields: {
                    withCredentials: true
                },
                success:function (data) {

                    if(data.error) {  // If there is an error, show the error messages
                        console.log('error');
                    }
                    else { // If not, send them back to the home page
//                            alert(data);
                        //do logout things
                        console.log(data);
                        if(data.register=='KO'){
                            if(data.msg=='fail'){
                            navigator.notification.confirm(resource.msgFailRegister, null, resource.msgErrorTitle, ["OK"]);
                            }else{
                            navigator.notification.confirm(resource.msgErrorRegister, null, resource.msgErrorTitle, ["OK"]);
                            }
                        }else{
                         navigator.notification.confirm(resource.msgRegisterationThanks, null, resource.msgTitle, ["OK"]);   
                        }

                    }
                },
                error:function(err){

                 alert('error');   
                }
            });
            
            
        },
        
        render: function(){
//            alert(
            var compiledTemplate = _.template(template);
            this.$el.html(compiledTemplate());
            
            this.loadModal();
            this.loadBeforeLoginTopMenu();
            
            this.loadMenu();
            this.rackView = new RackView($('.content'));
            this.loadCategoryRack(sessionStorage.getItem('current-category'));//this.loadCategoryRack(<DEFAULT CATEGORY>);
           
            
        },
        test:function(){
            console.log('test MasterView'); 
        }
 
  });
  // Our module now returns our view
  return MasterView;
});