var resource = {
//		rootUrl : 'http://127.0.0.1:53580',
        rootUrl :'http://localhost:8888/server_site(z)/laravel/public/api/dbp',
        rootPublicUrl:'http://localhost:8888/server_site(z)/laravel/public',
        rootFile:'idbp_theme',
        idbpEmail:'idbpv1@gmail.com',
        dbpWeb:'http://laman.dbp.gov.my',
    
//sample    
        sampleBookListUrl: '/sample_data/booklist_',
        samplebook:'/sample_data/book_',

//msg and String
        msg404Error:'Ralat. Tiada informasi pada permintaan.',
        msgEmptyListCategory:'Maaf, tiada senarai buku untuk kategori',
        msgPDFError:'Sila muat turun aplikasi bacaan format PDF untuk melihat',
        msgTitle:'Mesej',
        msgErrorTitle:'Ralat',
        msgWrongAuth:'Nama pengguna atau kata laluan yang dimasukkan tidak sah',
        msgErrorRegister:'Pastikan maklumat yang dimasukkan betul',
        msgFailRegister:'E-Mel anda telah didaftarkan sebelum ini',
        msgMinPassword:'Minimum 6 karakter', 
        msgMinUsername:'Minimum 5 karakter',
        msgRegisterationThanks:'Terima kasih kerana mendaftar',
        downloadTitle:'Muat turun sampel',
        msgDownload:'Pilih jenis format muat turun',
    
        txtLogin:'Rakam Masuk',
        txtUsername:'Nama pengguna',
        txtFullName:'Nama penuh',
        txtEmail:"E-Mel",
        txtPassword:"Kata laluan",
        txtRegister:"Daftar Pengguna Baru",
        txtUser:"Pengguna",
        txtLogout:"Keluar",
        txtForgot:"Lupa Kata Laluan",
        txtSend:"Hantar",
    
//item
        imagePlaceholder:'http://placehold.it/90x160/b&text=placeholder',
    
//server
        category:'/category/all',
        prefixStart:'&start=',
        prefixCategory:'&cat=',
        prefixExcept:'&except=',
        book:'/book/',
        login:'/login',
        logout:'/logout',
        register:'/register',
        dowload:'/downloads/book/',
        forgotPassword:'/password/forgot'
}