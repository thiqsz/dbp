// Filename: router.js
define([
  'jquery',
  'underscore',
  'backbone',
  'module/master/master'
    
], function($, _, Backbone,MasterView){
    
    var initialize = function(){
        console.log('OZdev');
        var masterView = new MasterView();
        masterView.render();
    };
  
    var AppRouter = Backbone.Router.extend({
        initialize:initialize,
  }); 
      

    
   return AppRouter;
});
