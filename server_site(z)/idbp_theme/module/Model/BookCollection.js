define([
  'underscore',
  'backbone',
  'module/Model/BookModel',
], function(_, Backbone,Book) {

  var BookCollection = Backbone.Collection.extend({

  		
       initialize: function(cat,start) {
           this.category='';
           
           if(!Number.isInteger(start)){
               this.start=0;
           }else{
               this.start= start; 
           }
           
        
           console.log('Catalogue Collection');
           
           if(cat!='undefined'){
               this.category = cat;
           }
           
          
  			
          
  		},

      models:Book,

      url : function() {
          console.log(resource.rootUrl+resource.category+'?'+resource.prefixStart+'='+this.start+'&cat='+this.category);
          return resource.rootUrl+resource.category+'?'+resource.prefixStart+'='+this.start+'&cat='+this.category;
	    },
     
      parse: function(response) {
        console.log('data parsed');
           return response;
      },	
      
      sync: function(method, model, options){  
        options.timeout = 10000;  
        options.dataType = "jsonp"; 
        return Backbone.sync(method, model, options);  
      } 
     
	    
    });
    
    

  	return BookCollection;

});