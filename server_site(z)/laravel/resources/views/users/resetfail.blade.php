<style>
.alert-box {
    color:#555;
    border-radius:10px;
    font-family:Tahoma,Geneva,Arial,sans-serif;font-size:11px;
    padding:10px 10px 10px 36px;
    margin:10px;
}

.alert-box span {
    font-weight:bold;
    text-transform:uppercase;
}
.warning {
    background:#fff8c4 url('images/warning.png') no-repeat 10px 50%;
    border:1px solid #f5aca6;
}
</style>
<div style="font-size:12.8000001907349px;line-height:normal;direction:ltr;font-family:Open sans,Arial,sans-serif;padding:1.5em;border-radius:1em;max-width:580px;margin:8.671875px auto 0px"><br>
<table style="width:395px;background-image:initial;background-repeat:initial">
<tbody>
<tr>
<td style="font-family:arial,sans-serif;margin:0px">
<div style="min-height:54px;margin:10px auto"><br>
@if($errors->any())
    <div class="alert-box warning">{{$errors->first()}}</div>
@endif
</div>
<div style="width:350.09375px;padding-bottom:10px;padding-left:15px">
<p><span style="font-family:Open sans,Arial,sans-serif;font-size:2.08em">Pembaharuan kata laluan.</span></p>
</div>
<div style="vertical-align:middle;padding:10px;max-width:398px;float:left">
<table style="vertical-align:middle">
<tbody>
<tr>
<td style="font-family:Open sans,Arial,sans-serif;margin:0px">
	<p>
		{{$message}}.
	</p>
</td>
</tr>
</tbody>
</table>
</div>
<div style="float:left;clear:both;padding:0px 5px 10px 10px"></div>
<br>
<div style="float:left;clear:both;padding:0px 5px 10px 10px"></div>
<div style="vertical-align:middle;padding:10px;max-width:398px;float:left">
<table style="vertical-align:middle">
<tbody>
<tr>

</tr>
</tbody>
</table>
</div>
<br>
<br>
<div style="clear:both;padding-left:13px;min-height:6.8em">
<table style="width:376px;border-collapse:collapse;border:0px">
<tbody>
<tr>
<td style="margin:0px;width:68px"><img alt="iDBP icon"  src="https://lh4.googleusercontent.com/-RQ8jY7p7OEk/AAAAAAAAAAI/AAAAAAAAAAA/h2i2EChv8Wg/photo.jpg?sz=60" style="display:block" width="49"></td>
<td style="font-family:Open sans,Arial,sans-serif;margin:0px;vertical-align:bottom"><span style="font-size:small">Dewan Bahasa &amp; Pustaka,<br>
</span><font size="5"><span style="line-height: 24px;">iDBP</span></font></td>
</tr>
</tbody>
</table>
</div>
</td>
</tr>
</tbody>
</table>
</div>
<div style="line-height: normal; color: rgb(119, 119, 119); font-size: 0.8em; border-radius: 1em; padding: 1em; margin: 0px auto 17.34375px; font-family: Arial, Helvetica, sans-serif; text-align: center; background-color: rgb(229, 229, 229);">© 2015 iDBP, Dewan Bahasa &amp; Pustaka</div>
