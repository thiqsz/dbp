<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Dewan Bahasa &amp; Pustaka</title>
    <META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">

    <link href="{{ URL::asset('css/font-awesome.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/bootstrap.min.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/animate.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/admin.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ URL::asset('css/jquerysctipttop.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('plugins/kalendar/kalendar.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('plugins/dropzone/dropzone.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('css/css-scroller/jquery.mCustomScrollbar.css') }}" rel="stylesheet">
     <link href="{{URL::asset('css/bootstrap-multiselect.css')}}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="{{ URL::asset('plugins/scroll/nanoscroller.css') }}">
    <link href="{{ URL::asset('plugins/morris/morris.css') }}" rel="stylesheet"/>
    <script src="{{ URL::asset('js/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('js/bootstrap.min.js') }}"></script>
</head>