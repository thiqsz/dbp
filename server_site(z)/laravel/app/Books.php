<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Books extends Model {

	protected $table = 'dbp_books';

	public function authors()
    {
        return $this->belongsToMany('App\Author','dbp_books_dbp_authors','dbp_books_id','dbp_authors_author_id');
    }

    public function samples()
    {
    	return $this->hasMany('App\BooksImages','books_id');
    }

    public function categories()
    {
    	return $this->belongsToMany('App\Category','dbp_books_dbp_category','dbp_books_id','dbp_category_cat_id');
    }


}
