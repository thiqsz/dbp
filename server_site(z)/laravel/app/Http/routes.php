<?php
use App\Books;
use App\Author;
use App\Category;
use App\BookLevel;
use App\Publishers;
use App\DBPStatus;
use App\BooksImages;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\View;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/login', 'HomeController@showLogin');
Route::post('/login','HomeController@doLogin');
Route::get('/register', 'HomeController@showRegister');
Route::post('/register', 'HomeController@toRegister');
Route::get('/verify/{token}','UsersAllController@verifyEmail');
Route::get('/password/change/{token}/proceed','UsersAllController@viewChangePassword');
Route::post('/password/change','UsersAllController@doChangePassword');

Route::filter('auth', function()
{
    if (Auth::guest()){
    	if(Request::is('api*')){
    		$msg = array('msg'=>'Please login');
    		return response()->json($msg)
        	->setCallback(Request::input('callback'));
    	}
    	return Redirect::to('login');
    } 
});




Route::filter('auth.basic', function()
{
    return Auth::basic();
});

Route::filter('guest', function()
{
    if (Auth::check()) return Redirect::to('dashboard');
});
Route::group(array('before' => 'auth'), function(){
    Route::resource('/', 'DashboardController');

    Route::resource('/dashboard',  'DashboardController');

    Route::resource('/books/book',  'BooksBookController');

    Route::get('/users/members',  'UsersAllController@index');

    Route::get('/users/admins',  'UsersAllController@adminIndex');

    Route::resource('/users/member',  'UsersAllController');

    Route::resource('/books/category',  'BooksCategoryController');

    Route::resource('/books/author',  'BooksAuthorController');

    Route::resource('/books/publisher',  'BooksPublisherController');

    Route::resource('/books/level', 'BooksLevelController');

    Route::get('/logout',  'HomeController@doLogout');

    Route::resource('/books/flipbook', 'FlipBookController');

    

});

//epub and pdf
Route::get('/downloads/book/{id}/{format}','BooksBookController@downloadBook'); //!!!!!!!!!!should be in session

// Route group for AJAX (SERVER-ADMIN) with auth
Route::group(array('prefix' => 'ajax','before'=>'auth'), function()
{
        //update sample upload
       Route::post('/book/{id}/uploadSample','BooksBookController@updateUploadSample');
       
       //delete all sample upload
       Route::get('/book/{id}/uploadSample/deleteAll','BooksBookController@deleteAllUploadedSample');

});


//MOBILE API
// open route
Route::group(array('prefix' => 'api/dbp'), function()
{

    Route::get('category/all', 'APIServiceController@category');
    Route::get('book/{id}', 'APIServiceController@bookDetails');

});



// Route with XHR handler
Route::group(array('middleware'=>'cors','prefix' => 'api/dbp'), function()
{
    Route::resource('login', 'APIServiceController@login');
    Route::resource('logout', 'APIServiceController@logout');
    Route::resource('register','APIServiceController@register');
    Route::post('password/forgot','APIServiceController@forgotPassword');

});

